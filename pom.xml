
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>fr.ifremer.common</groupId>
    <artifactId>common-parent</artifactId>
    <version>3.0.9</version>
  </parent>

  <groupId>fr.ifremer.dali</groupId>
  <artifactId>dali-ui-swing-launcher</artifactId>
  <version>2.1</version>
  <packaging>jar</packaging>
  
  <name>Dali :: UI Launcher</name>

  <url>http://doc.e-is.pro/dali</url>
  <inceptionYear>2018</inceptionYear>
  <organization>
    <name>Ifremer</name>
    <url>http://www.ifremer.fr</url>
  </organization>

  <licenses>
    <license>
      <name>Affero General Public License (AGPL)</name>
      <url>http://www.gnu.org/licenses/agpl.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <developers>
    <developer>
      <id>lp1ee9d</id>
      <name>Ludovic Pecquot</name>
      <email>ludovic.pecquot at e-is dot pro</email>
      <organization>EIS</organization>
      <organizationUrl>http://www.e-is.pro</organizationUrl>
      <roles>
        <role>developer</role>
      </roles>
      <timezone>Europe/Paris</timezone>
    </developer>
  </developers>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <platform>ifremer.fr</platform>
    <projectId>${project.artifactId}</projectId>
    <ciViewId>${project.artifactId}</ciViewId>
    <javaVersion>1.8</javaVersion>
    <signatureArtifactId>java18</signatureArtifactId>
    <signatureVersion>1.0</signatureVersion>

    <target.fileName>Dali.exe</target.fileName>

    <!-- Connexion using port redirection - https://domicile.ifremer.fr -->
    <distribution.site.host>127.0.0.1:4022</distribution.site.host>

    <distribution.repository.id>ifremer-public-repository</distribution.repository.id>
    <distribution.repository.url>scp://${distribution.site.host}/home/wwwinter/www/htdocs/maven/repository</distribution.repository.url>

    <distribution.snapshotRepository.id>ifremer-public-snapshots</distribution.snapshotRepository.id>
    <distribution.snapshotRepository.url>scp://${distribution.site.host}/home/wwwinter/www/htdocs/maven/snapshots</distribution.snapshotRepository.url>

    <distribution.internet.url>http://www.ifremer.fr/maven/repository</distribution.internet.url>

  </properties>

  <dependencies>
    <!-- please, no dependency -->
  </dependencies>

  <!-- Repositories needed to find the dependencies -->
  <repositories>
    <repository>
      <id>dali-public-group</id>
      <url>https://nexus.e-is.pro/nexus/content/groups/dali</url>
      <snapshots>
        <enabled>true</enabled>
        <checksumPolicy>fail</checksumPolicy>
      </snapshots>
      <releases>
        <enabled>true</enabled>
        <checksumPolicy>fail</checksumPolicy>
      </releases>
    </repository>
  </repositories>

  <profiles>
    <profile>
      <id>windows-launcher-bundle</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
        <os>
          <family>windows</family>
        </os>
        <activeByDefault>true</activeByDefault>
      </activation>
      <build>
        <defaultGoal>package</defaultGoal>
        <plugins>

          <plugin>
            <groupId>com.akathist.maven.plugins.launch4j</groupId>
            <artifactId>launch4j-maven-plugin</artifactId>
            <version>1.5.2</version>
            <executions>
              <execution>
                <id>l4j-clui</id>
                <phase>package</phase>
                <goals>
                  <goal>launch4j</goal>
                </goals>
                <configuration>
                  <headerType>gui</headerType>
                  <jar>${project.build.directory}/${project.artifactId}-${project.version}.jar</jar>
                  <outfile>${project.build.directory}/${target.fileName}</outfile>
                  <downloadUrl>http://java.com/download</downloadUrl>
                  <chdir>.</chdir>
                  <classPath>
                    <mainClass>fr.ifremer.dali.ui.swing.launcher.Launcher</mainClass>
                    <preCp>anything</preCp>
                  </classPath>
                  <icon>src/main/resources/dali.ico</icon>
                  <customProcName>true</customProcName>
                  <jre>
                    <path>jre</path>
                    <jdkPreference>preferJre</jdkPreference>
                    <maxHeapSize>20</maxHeapSize>
                  </jre>
                  <versionInfo>
                    <fileVersion>1.0.0.0</fileVersion>
                    <txtFileVersion>${project.version}</txtFileVersion>
                    <fileDescription>${project.name}</fileDescription>
                    <copyright>${project.inceptionYear} - ${project.organization.name}</copyright>
                    <productVersion>1.0.0.0</productVersion>
                    <txtProductVersion>${project.version}</txtProductVersion>
                    <productName>${project.name}</productName>
                    <companyName>${project.organization.name}</companyName>
                    <internalName>${project.name}</internalName>
                    <originalFilename>${target.fileName}</originalFilename>
                  </versionInfo>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-install-plugin</artifactId>
            <executions>
              <execution>
                <id>install_dali_exe</id>
                <phase>package</phase>
                <goals>
                  <goal>install-file</goal>
                </goals>
                <configuration>
                  <file>${project.build.directory}/${target.fileName}</file>
                  <packaging>exe</packaging>
                  <classifier>bin</classifier>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-deploy-plugin</artifactId>
            <executions>
              <execution>
                <id>deploy_dali_exe</id>
                <phase>deploy</phase>
                <goals>
                  <goal>deploy-file</goal>
                </goals>
                <configuration>

                  <repositoryId>${distribution.repository.id}</repositoryId>
                  <url>${distribution.repository.url}</url>

                  <file>${project.build.directory}/${target.fileName}</file>
                  <groupId>${project.groupId}</groupId>
                  <artifactId>${project.artifactId}</artifactId>
                  <version>${project.version}</version>
                  <packaging>exe</packaging>
                  <classifier>bin</classifier>
                  <generatePom>false</generatePom>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <artifactId>maven-assembly-plugin</artifactId>
            <executions>
              <execution>
                <id>assembly-launcher</id>
                <phase>package</phase>
                <goals>
                  <goal>single</goal>
                </goals>
                <configuration>
                  <attach>true</attach>
                  <descriptors>
                    <descriptor>
                      src/main/assembly/launcher.xml
                    </descriptor>
                  </descriptors>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>net.nicoulaj.maven.plugins</groupId>
            <artifactId>checksum-maven-plugin</artifactId>
            <version>1.10</version>
            <executions>
              <execution>
                <id>standalone-checksum</id>
                <phase>install</phase>
                <goals>
                  <goal>files</goal>
                </goals>
                <configuration>
                  <attachChecksums>true</attachChecksums>
                  <csvSummary>false</csvSummary>
                  <fileSets>
                    <fileSet>
                      <directory>${project.build.directory}</directory>
                      <includes>
                        <include>${project.artifactId}-${project.version}-bin.zip</include>
                      </includes>
                    </fileSet>
                  </fileSets>
                  <algorithms>
                    <algorithm>SHA-256</algorithm>
                  </algorithms>
                </configuration>
              </execution>
            </executions>
          </plugin>

        </plugins>
      </build>
    </profile>

    <profile>
      <id>non-windows-launcher-bundle</id>
      <!-- dummy profile to avoid unix-based system to build windows exe file -->
      <activation>
        <os>
          <family>unix</family>
        </os>
      </activation>
    </profile>

    <profile>
      <id>eis-deploy</id>
      <properties>

        <distribution.repository.id>eis-nexus-deploy</distribution.repository.id>
        <distribution.repository.url>https://nexus.e-is.pro/nexus/content/repositories/dali-releases</distribution.repository.url>

        <distribution.snapshotRepository.id>eis-nexus-deploy</distribution.snapshotRepository.id>
        <distribution.snapshotRepository.url>https://nexus.e-is.pro/nexus/content/repositories/dali-snapshots</distribution.snapshotRepository.url>

        <distribution.internet.url>https://nexus.e-is.pro/nexus/content/groups/public</distribution.internet.url>

      </properties>
    </profile>

    <profile>
      <id>gitlab-deploy</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <properties>

        <!-- keep eis for site -->
        <distribution.site.id>eis-public-reports</distribution.site.id>
        <distribution.site.host>doc.e-is.pro</distribution.site.host>
        <distribution.site.path>/var/www/doc.e-is.pro</distribution.site.path>
        <distribution.site.repository>scpexe://${distribution.site.host}${distribution.site.path}</distribution.site.repository>

        <gitlab.projectId>quadrige%2Fdali-launcher</gitlab.projectId>

        <distribution.repository.id>dali-launcher-gitlab-deploy</distribution.repository.id>
        <distribution.repository.url>https://gitlab.ifremer.fr/api/v4/projects/${gitlab.projectId}/packages/maven</distribution.repository.url>

        <distribution.snapshotRepository.id>dali-gitlab-deploy</distribution.snapshotRepository.id>
        <distribution.snapshotRepository.url>https://gitlab.ifremer.fr/api/v4/projects/${gitlab.projectId}/packages/maven</distribution.snapshotRepository.url>

        <distribution.internet.url>https://gitlab.ifremer.fr/api/v4/projects/${gitlab.projectId}/packages/maven</distribution.internet.url>

      </properties>
    </profile>


  </profiles>
</project>

